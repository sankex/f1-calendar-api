<?php
    use Sankex\F1calendar\Models\Race;

    Route::get('/', function() {
        $races = Race::with('flag')->get();
        return $races;
    });

    Route::get('races', function() {
        $races = Race::with('flag')->get();
        return $races;
    });

    Route::get('races/{id}', function ($id) {
        $race = Race::where('country_code', $id)->first();
        return $race;
    });

    Route::get('/404', function() {
        return response()->json([
            'error' => 'Not Found', 
            'status' => '404', 
            'message' => 'Page Not Found'
        ]);
    });

    Route::get('/error', function() {
        return response()->json([
            'error' => 'Not Found', 
            'status' => '404', 
            'message' => 'Page Not Found'
        ]);
    });