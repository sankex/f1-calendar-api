<?php namespace Sankex\F1calendar\Models;

use Model;

/**
 * Model
 */
class Race extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $attachOne = [
        'flag' => 'System\Models\File'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'sankex_f1calendar_2019_races';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
