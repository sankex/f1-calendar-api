<?php namespace Sankex\F1calendar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSankexF1calendar2019Races extends Migration
{
    public function up()
    {
        Schema::create('sankex_f1calendar_2019_races', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('race_title');
            $table->dateTime('fp1_timestamp');
            $table->dateTime('fp2_timestamp');
            $table->dateTime('fp3_timestamp');
            $table->dateTime('qual_timestamp');
            $table->dateTime('race_timestamp');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('sankex_f1calendar_2019_races');
    }
}
