<?php namespace Sankex\F1calendar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSankexF1calendar2019Races2 extends Migration
{
    public function up()
    {
        Schema::table('sankex_f1calendar_2019_races', function($table)
        {
            $table->boolean('active');
            $table->string('race_title')->change();
            $table->string('country_code')->change();
        });
    }
    
    public function down()
    {
        Schema::table('sankex_f1calendar_2019_races', function($table)
        {
            $table->dropColumn('active');
            $table->string('race_title', 191)->change();
            $table->string('country_code', 191)->change();
        });
    }
}
