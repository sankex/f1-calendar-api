<?php namespace Sankex\F1calendar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSankexF1calendar2019Races extends Migration
{
    public function up()
    {
        Schema::table('sankex_f1calendar_2019_races', function($table)
        {
            $table->string('country_code');
            $table->increments('id')->unsigned(false)->change();
            $table->string('race_title')->change();
        });
    }
    
    public function down()
    {
        Schema::table('sankex_f1calendar_2019_races', function($table)
        {
            $table->dropColumn('country_code');
            $table->increments('id')->unsigned()->change();
            $table->string('race_title', 191)->change();
        });
    }
}
