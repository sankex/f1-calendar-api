<?php namespace Sankex\F1calendar\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Races extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Sankex.F1calendar', 'f1-calendar');
    }
}
